package com.zzq;

import com.zzq.singleton.Singleton;

import java.util.Arrays;

/**
 * @description:
 * @author: zzq
 * @create: 2021-12-22 10:57
 * @Version 1.0
 **/


@Foo(value = {"sherman", "decompiler"}, bar = true)
public class Demo {
    public static void main(String[] args) {

        Singleton instance1 = Singleton.getInstance();
        Singleton instance2 = Singleton.getInstance();

        System.out.println(instance1==instance2);

        //Foo foo = Demo.class.getAnnotation(Foo.class);
        ////注释
        //System.out.println(Arrays.toString(foo.value()));
    }


}

