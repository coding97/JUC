package com.zzq;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * @author 队长
 */
public class Txt {
    static class Student {
        private String name;
        private int grade;


        public Student(String name, int grade) {
            super();
            this.name = name;
            this.grade = grade;
        }


        public Student() {
            super();
        }


        public String getName() {
            return name;
        }


        public void setName(String name) {
            this.name = name;
        }


        public int getGrade() {
            return grade;
        }


        public void setGrade(int grade) {
            this.grade = grade;
        }


        @Override
        public String toString() {
            return "Student [name=" + name + ", grade=" + grade + "]";
        }


    }

    public static void main(String[] args) throws IOException {



        BufferedReader br = new BufferedReader(new FileReader("A.txt"));
        // 创建集合
        List<Student> ar = new ArrayList<>();
        String line;
        // 标题行
        String title = null;
        int in = 0;
        Student student = null;
        while ((line = br.readLine()) != null) {
            // 每次循环创建新的学生对象
            student = new Student();
            // 获取标题行
            if (in == 0) {
                title = line;
                in = 1;
            } else {
                // 切割字符串
                String[] strAy = line.split(",");
                // 设置属性
                student.setName(strAy[0]);
                student.setGrade(Integer.parseInt(strAy[1]));
                ar.add(student);
            }

        }

        br.close();


        // 集合排序使用匿名内部类Collections
        Collections.sort(ar, Comparator.comparingInt(Student::getGrade));


        // 写进指定文件
        BufferedWriter bw = new BufferedWriter(new FileWriter("B.txt"));
        // 写入标题行
        bw.write(title + "\n");
        for (Student s : ar) {
            bw.write(s.getName() + "   " + s.getGrade());
            bw.newLine();

        }
        bw.write("--------"+"\n");
        double asDouble = ar.stream().mapToInt(Student::getGrade).average().getAsDouble();
        bw.write("均分" + "   " + asDouble);
        bw.close();
    }


}

