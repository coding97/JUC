package com.zzq;

import java.util.Scanner;

/**
 * @author zzq
 * @date 2021年12月28日 10:48
 */

public class HasStatic {


    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int N = Integer.parseInt(sc.nextLine());
        while (N-- > 0) {
            String str = sc.nextLine();
            StringBuilder s = new StringBuilder(str);
            for (int i = 0; i + 2 < s.length(); ++i) {
                char a = s.charAt(i), b = s.charAt(i + 1), c = s.charAt(i + 2);
                if (a == b && b == c) {
                    s.deleteCharAt(i--);
                }
            }
            for (int i = 0; i + 3 < s.length(); ++i) {
                char a = s.charAt(i), b = s.charAt(i + 1), c = s.charAt(i + 2), d = s.charAt(i + 3);
                if (a == b && c == d) {
                    s.deleteCharAt(i + 2);
                }
            }
            System.out.println(s);
        }
    }


}
