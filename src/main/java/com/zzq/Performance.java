package com.zzq;

/**
 * @author zzq
 * @date 2021年12月22日 13:25
 */

public interface Performance {

    void perform();
}
