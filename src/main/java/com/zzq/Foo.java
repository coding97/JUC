package com.zzq;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author 队长
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Foo{
    String[] value();
    boolean bar();
}
