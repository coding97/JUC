package com.zzq;

import java.util.LinkedList;

/**
 * @author zzq
 * @date 2022年01月17日 17:10
 */

public class MinStack {

    public ListNode reversePrint(ListNode head) {

        ListNode pre = null, cur = head, next = null;
        while (cur != null) {
            next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }

}

class ListNode {
    int val;

    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
