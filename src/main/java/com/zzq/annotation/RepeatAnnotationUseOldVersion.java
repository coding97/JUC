package com.zzq.annotation;

/**
 * @author zzq
 * @date 2022年01月20日 13:51
 */

public class RepeatAnnotationUseOldVersion {

    @Authorities({@Authority(role="Admin"),@Authority(role="Manager")})
    public void doSomeThing(){
    }
}
