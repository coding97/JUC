package com.zzq.annotation;

/**
 * @author 队长
 */
public @interface Authority {
    String role();
}
