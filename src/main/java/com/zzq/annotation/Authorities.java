package com.zzq.annotation;

/**
 * @author 队长
 */
public @interface Authorities {
    Authority[] value();
}
